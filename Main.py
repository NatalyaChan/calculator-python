from tkinter import *
import math

X = 0
switch = bool(0)
switchFinish = bool(0)
withpoint = bool(0)
Op = ''
PreLE = ''


def SwitchOff():  # рычаг опущен. Необработанных чисел нет, результат есть
    global switch
    switch = bool(0)


def SwitchOn():  # рычаг активирован, экран очищен, в него введено новое необработанное число
    global switch
    if (switch == bool(0)) and (withpoint == bool(0)):
        LabelDisplay['text'] = ''
    switch = bool(1)


def ShowCurrentResult():
    global switch
    if switch == bool(1):
        Finish()


def CheckNewExpression():
    global switchFinish
    global switch
    if switchFinish == bool(1):
        switch = bool(1)
        switchFinish = bool(0)


def AddOpExpression():
    global switch
    global Op
    global PreLE
    if switch == bool(1):
        PreLE = LabelExpression['text']
        LabelExpression['text'] = AOE(LabelExpression['text'])
    else:
        LabelExpression['text'] = AOE(PreLE)


def AOE(le):
    if ((Op == 'x') or (Op == '/')) and (('+' in PreLE) or ('-' in PreLE)):
        z = '(' + le + ')' + Op
    else:
        z = le + Op
    return z


def RemoveWasteDecimalPlace(x):
    z = str(x)
    if (float(z) == math.trunc(float(z))):
        z = str(math.trunc(float(z)))
    return z


def ClickButton1():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '1'


def ClickButton2():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '2'


def ClickButton3():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '3'


def ClickButton4():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '4'


def ClickButton5():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '5'


def ClickButton6():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '6'


def ClickButton7():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '7'


def ClickButton8():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '8'


def ClickButton9():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '9'


def ClickButton0():
    SwitchOn()
    CheckNewExpression()
    LabelDisplay['text'] = LabelDisplay['text'] + '0'


def AddDisplaytoExpression():
    global PreLE
    if (float(LabelDisplay['text']) < 0):
        LabelExpression['text'] = LabelExpression['text'] + '(' + LabelDisplay['text'] + ')'
    else:
        LabelExpression['text'] = LabelExpression['text'] + LabelDisplay['text']


def OpInProgress():
    global X
    CheckNewExpression()
    if switch == bool(1):
        AddDisplaytoExpression()
    ShowCurrentResult()
    if (LabelDisplay['text'] != 'Зачем вы делите на 0?'):
        X = float(LabelDisplay['text'])


def ClickButtonPlus():
    global Op
    OpInProgress()
    Op = '+'
    AddOpExpression()
    SwitchOff()


def ClickButtonMinus():
    global Op
    OpInProgress()
    Op = '-'
    AddOpExpression()
    SwitchOff()


def ClickButtonMulti():
    global Op
    OpInProgress()
    Op = 'x'
    AddOpExpression()
    SwitchOff()


def ClickButtonDivi():
    global Op
    OpInProgress()
    Op = '/'
    AddOpExpression()
    SwitchOff()


def ClickButtonPoint():
    global X
    global withpoint
    CheckNewExpression()
    if switch == bool(1) and (LabelDisplay['text'].find('.') == -1):
        withpoint = bool(1)
        LabelDisplay['text'] = LabelDisplay['text'] + '.'


def ClickButtonSign():
    global switch
    global PreLE
    CheckNewExpression()
    LabelDisplay['text'] = str(float(LabelDisplay['text']) * (-1))
    if (switch == bool(0)):
        LabelExpression['text'] = '(' + PreLE + ')' + '*(-1)'
    LabelDisplay['text'] = RemoveWasteDecimalPlace(LabelDisplay['text'])
    PreLE = LabelExpression['text']


def ClickButtonSquare():
    global switch
    global PreLE
    global Op
    OpInProgress()
    Op = '^2'
    LabelDisplay['text'] = RemoveWasteDecimalPlace(float(LabelDisplay['text']) ** 2)
    if switch == bool(1):
        LabelExpression['text'] = 'sqr(' + LabelExpression['text'] + ')'
    else:
        LabelExpression['text'] = 'sqr(' + PreLE + ')'
    PreLE = LabelExpression['text']
    SwitchOff()


def ClickButtonSquareRoot():
    global PreLE
    global Op
    OpInProgress()
    Op = 'sqrt'
    if (float(LabelDisplay['text']) < 0):
        LabelExpression['text'] = 'Этот калькулятор \nне работает с комплексными \nчислами'
        ButtonError.place(x=8, y=80)
        return
    else:
        LabelDisplay['text'] = RemoveWasteDecimalPlace(math.sqrt(float(LabelDisplay['text'])))
    if switch == bool(1):
        LabelExpression['text'] = 'sqrt(' + LabelExpression['text'] + ')'
    else:
        LabelExpression['text'] = 'sqrt(' + PreLE + ')'
    PreLE = LabelExpression['text']
    SwitchOff()


def ClickButtonReverse():
    global PreLE
    global Op
    OpInProgress()
    Op = '1/'
    if (float(LabelDisplay['text']) == 0):
        LabelDisplay['text'] = 'Зачем вы делите на 0?'
        ButtonError.place(x=8, y=80)
        return
    else:
        LabelDisplay['text'] = RemoveWasteDecimalPlace(1 / (float(LabelDisplay['text'])))
    if switch == bool(1):
        LabelExpression['text'] = '1/(' + LabelExpression['text'] + ')'
    else:
        LabelExpression['text'] = '1/(' + PreLE + ')'
    PreLE = LabelExpression['text']
    SwitchOff()


def ClickButtonPercent():
    LabelDisplay['text'] = RemoveWasteDecimalPlace(X * float(LabelDisplay['text']) / 100)


def ClickButtonR():
    global PreLE
    global switchFinish
    Finish()
    LabelExpression['text'] = ''
    PreLE = LabelDisplay['text']
    SwitchOff()
    switchFinish = bool(1)


def Finish():
    global Op
    global withpoint
    global PreLE
    withpoint = bool(0)
    if (switch == bool(0)):
        LabelExpression['text'] = PreLE
    elif Op == '+':
        LabelDisplay['text'] = str(X + float(LabelDisplay['text']))

    elif Op == '-':
        LabelDisplay['text'] = str(X - float(LabelDisplay['text']))
    elif Op == 'x':
        LabelDisplay['text'] = str(X * float(LabelDisplay['text']))
    elif Op == '/':
        if (float(LabelDisplay['text']) == 0):
            LabelDisplay['text'] = 'Зачем вы делите на 0?'
            ButtonError.place(x=8, y=80)
            return
        else:
            LabelDisplay['text'] = str(X / float(LabelDisplay['text']))
    elif (Op == '^2') or (Op == 'sqrt') or (Op == '1/'):
        PreLE = LabelExpression['text']
    Op = ''
    LabelDisplay['text'] = RemoveWasteDecimalPlace(LabelDisplay['text'])


def ClickClear():
    global X
    global Op
    global switch
    global switchFinish
    global withpoint
    global PreLE
    X = 0
    switch = bool(0)
    switchFinish = bool(0)
    withpoint = bool(0)
    Op = ''
    PreLE = ''
    LabelDisplay['text'] = '0'
    LabelExpression['text'] = ''


def ClickCE():
    global switch
    LabelDisplay['text'] = '0'
    switch = bool(0)


FormMain = Tk()
FormMain.title("Калькулятор")
FormMain.geometry('194x200')
FormMain.configure(bg="#d6e4ff")

LabelDisplay = Label(FormMain, text='0', font="Arial 10", anchor='se', bg="#ebf1ff", relief="sunken", height='4',
                     width='21')
LabelDisplay.place(x=10, y=5)
LabelExpression = Label(FormMain, text='', anchor='se', bg="#ebf1ff", width='27', font="Arial 8")
LabelExpression.place(x=14, y=8)
Button1 = Button(FormMain, text='1', command=ClickButton1, height='1', width='2', bg='#b8cfff')
Button1.place(x=10, y=140)
Button2 = Button(FormMain, text='2', command=ClickButton2, height='1', width='2', bg='#b8cfff')
Button2.place(x=40, y=140)
Button3 = Button(FormMain, text='3', command=ClickButton3, height='1', width='2', bg='#b8cfff')
Button3.place(x=70, y=140)
Button4 = Button(FormMain, text='4', command=ClickButton4, height='1', width='2', bg='#b8cfff')
Button4.place(x=10, y=110)
Button5 = Button(FormMain, text='5', command=ClickButton5, height='1', width='2', bg='#b8cfff')
Button5.place(x=40, y=110)
Button6 = Button(FormMain, text='6', command=ClickButton6, height='1', width='2', bg='#b8cfff')
Button6.place(x=70, y=110)
Button7 = Button(FormMain, text='7', command=ClickButton7, height='1', width='2', bg='#b8cfff')
Button7.place(x=10, y=80)
Button8 = Button(FormMain, text='8', command=ClickButton8, height='1', width='2', bg='#b8cfff')
Button8.place(x=40, y=80)
Button9 = Button(FormMain, text='9', command=ClickButton9, height='1', width='2', bg='#b8cfff')
Button9.place(x=70, y=80)
Button0 = Button(FormMain, text='0', command=ClickButton0, height='1', width='2', bg='#b8cfff')
Button0.place(x=40, y=170)
ButtonPoint = Button(FormMain, text='.', command=ClickButtonPoint, height='1', width='2', bg='#a3c2ff')
ButtonPoint.place(x=10, y=170)
ButtonSign = Button(FormMain, text='+-', command=ClickButtonSign, height='1', width='2', bg='#a3c2ff')
ButtonSign.place(x=70, y=170)
ButtonPlus = Button(FormMain, text='+', command=ClickButtonPlus, height='1', width='2', bg='#a3c2ff')
ButtonPlus.place(x=100, y=170)
ButtonMinus = Button(FormMain, text='-', command=ClickButtonMinus, height='1', width='2', bg='#a3c2ff')
ButtonMinus.place(x=100, y=140)
ButtonMulti = Button(FormMain, text='x', command=ClickButtonMulti, height='1', width='2', bg='#a3c2ff')
ButtonMulti.place(x=100, y=110)
ButtonDivi = Button(FormMain, text='/', command=ClickButtonDivi, height='1', width='2', bg='#a3c2ff')
ButtonDivi.place(x=100, y=80)
ButtonSquare = Button(FormMain, text='sqr', command=ClickButtonSquare, height='1', width='2', bg='#a3c2ff')
ButtonSquare.place(x=130, y=110)
ButtonSquareRoot = Button(FormMain, text='sqrt', command=ClickButtonSquareRoot, height='1', width='2', bg='#a3c2ff')
ButtonSquareRoot.place(x=130, y=140)
ButtonSquareReverse = Button(FormMain, text='1/x', command=ClickButtonReverse, height='1', width='2', bg='#a3c2ff')
ButtonSquareReverse.place(x=160, y=140)
ButtonPercent = Button(FormMain, text='%', command=ClickButtonPercent, height='1', width='2', bg='#a3c2ff')
ButtonPercent.place(x=160, y=110)
ButtonR = Button(FormMain, text='=', command=ClickButtonR, height='1', width='6', bg='#a3c2ff')
ButtonR.place(x=130, y=170)
ButtonC = Button(FormMain, text='C', command=ClickClear, height='1', width='2', bg='#a3c2ff')
ButtonC.place(x=160, y=80)
ButtonCE = Button(FormMain, text='CE', command=ClickCE, height='1', width='2', bg='#a3c2ff')
ButtonCE.place(x=130, y=80)

ButtonError = Button(FormMain, text='Exit', command=sys.exit, height='7', width='24', bg='#a3c2ff')

FormMain.mainloop()
#готово